\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\select@language {spanish}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\IeC {\'I}NDICE DE FIGURAS}{iv}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{CAP\IeC {\'I}TULO 1. \hspace {0.2cm} \uppercase {Introducci\IeC {\'o}n}}{5}
\defcounter {refsection}{0}\relax 
\contentsline {section}{1.1 \hspace {0.5cm}\uppercase {Motivaci\IeC {\'o}n}}{6}
\defcounter {refsection}{0}\relax 
\contentsline {section}{1.2 \hspace {0.5cm}\uppercase {Objetivo General}}{6}
\defcounter {refsection}{0}\relax 
\contentsline {section}{1.3 \hspace {0.5cm}\uppercase {Objetivos Espec\IeC {\'\i }ficos}}{6}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{CAP\IeC {\'I}TULO 2. \hspace {0.2cm} \uppercase {Descripci\IeC {\'o}n del problema}}{7}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{CAP\IeC {\'I}TULO 3. \hspace {0.2cm} \uppercase {Descripci\IeC {\'o}n del observatorio}}{9}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{CAP\IeC {\'I}TULO 4. \hspace {0.2cm} \uppercase {Descripci\IeC {\'o}n de entidades y atributos}}{11}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{CAP\IeC {\'I}TULO 5. \hspace {0.2cm} \uppercase {Modelo Entidad Relaci\IeC {\'o}n}}{17}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{CAP\IeC {\'I}TULO 6. \hspace {0.2cm} \uppercase {Conclusi\IeC {\'o}n}}{19}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{CAP\IeC {\'I}TULO 7. \hspace {0.2cm} \uppercase {Referencias}}{21}
