\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\select@language {spanish}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\IeC {\'I}NDICE DE FIGURAS}{v}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{CAP\IeC {\'I}TULO 1. \hspace {0.2cm} \uppercase {Introducci\IeC {\'o}n}}{7}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{CAP\IeC {\'I}TULO 2. \hspace {0.2cm} \uppercase {Objetivos}}{9}
\defcounter {refsection}{0}\relax 
\contentsline {section}{2.1 \hspace {0.5cm}\uppercase {Objetivo General}}{9}
\defcounter {refsection}{0}\relax 
\contentsline {section}{2.2 \hspace {0.5cm}\uppercase {Objetivos espec\IeC {\'\i }ficos}}{9}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{CAP\IeC {\'I}TULO 3. \hspace {0.2cm} \uppercase {Marco te\IeC {\'o}rico}}{11}
\defcounter {refsection}{0}\relax 
\contentsline {section}{3.1 \hspace {0.5cm}\uppercase {Descripci\IeC {\'o}n de contenidos}}{11}
\defcounter {refsection}{0}\relax 
\contentsline {section}{3.2 \hspace {0.5cm}\uppercase {Configuraciones de cach\IeC {\'e}}}{11}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{3.2.1 \hspace {0.2cm} Cach\IeC {\'e} mapeo directo}{11}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{3.2.2 \hspace {0.2cm} Cach\IeC {\'e} full asociativo}{12}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{3.2.3 \hspace {0.2cm} Cach\IeC {\'e} asociativo por bloques}{13}
\defcounter {refsection}{0}\relax 
\contentsline {section}{3.3 \hspace {0.5cm}\uppercase {Descripci\IeC {\'o}n de algoritmos a utilizar}}{13}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{3.3.1 \hspace {0.2cm} Algoritmo de Dijkstra}{13}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{3.3.2 \hspace {0.2cm} Algoritmo Best First Search}{14}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{CAP\IeC {\'I}TULO 4. \hspace {0.2cm} \uppercase {Desarrollo}}{15}
\defcounter {refsection}{0}\relax 
\contentsline {section}{4.1 \hspace {0.5cm}\uppercase {Procedimiento y descripci\IeC {\'o}n de la experiencia}}{15}
\defcounter {refsection}{0}\relax 
\contentsline {section}{4.2 \hspace {0.5cm}\uppercase {Pruebas Algoritmo Best First Search}}{16}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{4.2.1 \hspace {0.2cm} cach\IeC {\'e} mapeo directo con Best First Search}{16}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{4.2.2 \hspace {0.2cm} Cach\IeC {\'e} Full asociativo con Best First Search}{17}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{4.2.3 \hspace {0.2cm} Cach\IeC {\'e} Asociativo de n bloques con Best First Search}{18}
\defcounter {refsection}{0}\relax 
\contentsline {section}{4.3 \hspace {0.5cm}\uppercase {Pruebas algoritmo de Dijkstra}}{20}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{4.3.1 \hspace {0.2cm} Cach\IeC {\'e} Mapeo Directo con algoritmo de Dijkstra}{20}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{4.3.2 \hspace {0.2cm} Cach\IeC {\'e} Full Asociativo con algoritmo de Dijkstra}{21}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{4.3.3 \hspace {0.2cm} Cach\IeC {\'e} Asociativo de N bloques con algoritmo de Dijkstra}{22}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{CAP\IeC {\'I}TULO 5. \hspace {0.2cm} \uppercase {Discusiones}}{25}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{CAP\IeC {\'I}TULO 6. \hspace {0.2cm} \uppercase {Conclusi\IeC {\'o}n}}{27}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{CAP\IeC {\'I}TULO 7. \hspace {0.2cm} \uppercase {Bibliograf\IeC {\'\i }a}}{29}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{CAP\IeC {\'I}TULO 8. \hspace {0.2cm} \uppercase {Anexos}}{31}
