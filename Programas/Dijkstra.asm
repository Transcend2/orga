.data 0x10010000	      
Nodo1:	.word 0,1,3,0,0,0,0,0 #dependencias
Nodo2:	.word 0,0,0,2,0,5,0,0
Nodo3:  .word 0,0,0,1,5,0,0,0
Nodo4: 	.word 0,0,0,0,0,2,4,0
Nodo5:  .word 0,0,0,0,0,0,2,0
Nodo6:  .word 0,0,0,0,0,0,0,3
Nodo7:  .word 0,0,0,0,0,0,0,1
mensaje: .asciiz "El camino más corto es de longitud: "
.text

	insertar:	
		la $s1,Nodo1 #se asignan las listas a distintos registros
		la $s2,Nodo2
		la $s3,Nodo3
		la $s4,Nodo4
		la $s5,Nodo5
		la $s6,Nodo6
		la $s7,Nodo7
		
		la $t8,268501440 #Lista de valores acumulados en los nodos
		la $t9,268501472 #Lista de nodos anteriores 
		li $t7,8
		
	insert_1:
		beqz $t7,set_2 #al finalizar evaluación de nodo, se evalúa el siguiente
		subi $t7,$t7,1
		addi $s1,$s1,4
		lw $t0,0($s1)
		beqz $t0,insert_1 #si no hay valor, no tiene conexión con el nodo (se exaina siguiente)
		lw $t1,0($s1) #de otra forma se llena con el primer camino tentativo
		sw $t1,4($t8)
		addi $t8,$t8,4
		li $t1,1 #se asigna un 1 a los nodos a quienes preceda el nodo N°1 (para recordar camino)
		sw $t1,4($t9)
		addi $t9,$t9,4
		j insert_1
	set_2:
		li $t7,8
		la $t8,268501440 
		la $t9,268501472 
		j insert_2
		
	insert_2:
		beqz $t7,set_3 #al finalizar evaluación de nodo, se evalúa el siguiente
		subi $t7,$t7,1
		addi $s2,$s2,4
		addi $t8,$t8,4
		addi $t9,$t9,4
		lw $t0,0($t8)
		bnez $t0, evaluar_2 #si el nodo siguiente es distinto de 0 debe evaluarse 
		
		lw $t0,0($s2)
		beqz $t0,insert_2 #si no hay valor, no tiene conexión con el nodo
		li $t0,1 #valor acumulado de la rama (estático en este caso)
		lw $t1,0($s2) #de otra forma se llena con el primer camino tentativo
		add $t1,$t1,$t0 #sumado al acumulado del camino
		sw $t1,0($t8)
		addi $t8,$t8,4
		li $t1,2 #se marca camino con un 2
		sw $t1,0($t9)
		addi $t9,$t9,4
		j insert_2
		
	evaluar_2:
		lw $t2,0($s2)
		beqz $t2,insert_2 #Si es 0, se evalúa siguiente
		li $t1,1
		add $t1,$t1,$t2 #total camino tentativo
		bge $t1,$t0, insert_2 #si el camino tentativo es mayor al guardado, no se considera
		sw $t1,0($t8) #de otra forma, se guarda el valor del nuevo camino
		li $t1,2
		sw $t1,0($t9) #se guarda recorrido del nuevo camino
		j insert_2
	
	set_3:
		li $t7,8
		la $t8,268501440 
		la $t9,268501472 
		j insert_3
		
	insert_3:
		beqz $t7,set_4 #al finalizar evaluación de nodo, se evalúa el siguiente
		subi $t7,$t7,1
		addi $t8,$t8,4
		addi $s3,$s3,4
		addi $t9,$t9,4
		lw $t0,0($t8)
		bnez $t0, evaluar_3 #si el nodo siguiente es distinto de 0 debe evaluarse 
		lw $t0,0($s3)
		beqz $t0,insert_3 #si no hay valor, no tiene conexión con el nodo
		li $t0,3 #valor acumulado de la rama (estático en este caso)
		lw $t1,0($s3) #de otra forma se llena con el primer camino tentativo
		add $t1,$t1,$t0 #sumado al acumulado del camino
		sw $t1,0($t8)
		li $t1,3 #se marca camino con un 3
		sw $t1,0($t9)
		j insert_3
		
	evaluar_3:
		lw $t2,0($s3)
		beqz $t2,insert_3
		li $t1,3
		add $t1,$t1,$t2 #total camino tentativo
		bge $t1,$t0, insert_3 #si el camino tentativo es mayor al guardado, no se considera
		sw $t1,0($t8) #de otra forma, se guarda el valor del nuevo camino
		li $t1,3
		sw $t1,0($t9) #se guarda recorrido del nuevo camino
		j insert_3
		
	set_4:
		li $t7,8
		la $t8,268501440 
		la $t9,268501472 
		j insert_4
		
	insert_4:
		
		beqz $t7,set_5 #al finalizar evaluación de nodo, se evalúa el siguiente
		subi $t7,$t7,1
		addi $t8,$t8,4
		addi $s4,$s4,4
		addi $t9,$t9,4
		lw $t0,0($t8)
		bnez $t0, evaluar_4 #si el nodo siguiente es distinto de 0 debe evaluarse 
		lw $t0,0($s4)
		beqz $t0,insert_4 #si no hay valor, no tiene conexión con el nodo
		lw $t0,-8($t8) #valor acumulado de la rama 
		lw $t1,0($s4) #de otra forma se llena con el primer camino tentativo
		add $t1,$t1,$t0 #sumado al acumulado del camino
		sw $t1,0($t8)
		li $t1,4 #se marca camino con un 4
		sw $t1,0($t9)
		j insert_4
		
	evaluar_4:
		lw $t2,0($s4)
		beqz $t2,insert_4
		lw $t1,0($t8)
		add $t1,$t1,$t2 #total camino tentativo
		bge $t1,$t0, insert_4 #si el camino tentativo es mayor al guardado, no se considera
		sw $t1,0($t8) #de otra forma, se guarda el valor del nuevo camino
		li $t1,4
		sw $t1,0($t9) #se guarda recorrido del nuevo camino
		j insert_4
		
	set_5:
		li $t7,8
		la $t8,268501440 
		la $t9,268501472 
		j insert_5
		
	insert_5:
		
		beqz $t7,set_6 #al finalizar evaluación de nodo, se evalúa el siguiente
		subi $t7,$t7,1
		addi $t8,$t8,4
		addi $s5,$s5,4
		addi $t9,$t9,4
		lw $t0,0($t8)
		bnez $t0, evaluar_5 #si el nodo siguiente es distinto de 0 debe evaluarse 
		lw $t0,0($s5)
		beqz $t0,insert_5 #si no hay valor, no tiene conexión con el nodo
		lw $t0,8($t8) #valor acumulado de la rama 
		lw $t1,0($s5) #de otra forma se llena con el primer camino tentativo
		add $t1,$t1,$t0 #sumado al acumulado del camino
		sw $t1,0($t8)
		li $t1,5 #se marca camino con un 5
		sw $t1,0($t9)
		j insert_5
		
	evaluar_5:
		lw $t2,0($s5)
		beqz $t2,insert_5
		lw $t1,0($t8)
		add $t1,$t1,$t2 #total camino tentativo
		bge $t1,$t0, insert_5 #si el camino tentativo es mayor al guardado, no se considera
		sw $t1,0($t8) #de otra forma, se guarda el valor del nuevo camino
		li $t1,5
		sw $t1,0($t9) #se guarda recorrido del nuevo camino
		j insert_5
		
	set_6:
		li $t7,8
		la $t8,268501440 
		la $t9,268501472 
		j insert_6
		
	insert_6:
		
		beqz $t7,set_7 #al finalizar evaluación de nodo, se evalúa el siguiente
		subi $t7,$t7,1
		addi $t8,$t8,4
		addi $s6,$s6,4
		addi $t9,$t9,4
		lw $t0,0($t8)
		bnez $t0, evaluar_6 #si el nodo siguiente es distinto de 0 debe evaluarse 
		lw $t0,0($s6)
		beqz $t0,insert_6 #si no hay valor, no tiene conexión con el nodo
		lw $t0,-8($t8) #valor acumulado de la rama 
		lw $t1,0($s6) #de otra forma se llena con el primer camino tentativo
		add $t1,$t1,$t0 #sumado al acumulado del camino
		sw $t1,0($t8)
		li $t1,6 #se marca camino con un 6
		sw $t1,0($t9)
		j insert_5
		
	evaluar_6:
		lw $t2,0($s6)
		beqz $t2,insert_6
		lw $t1,0($t8)
		add $t1,$t1,$t2 #total camino tentativo
		bge $t1,$t0, insert_6 #si el camino tentativo es mayor al guardado, no se considera
		sw $t1,0($t8) #de otra forma, se guarda el valor del nuevo camino
		li $t1,6
		sw $t1,0($t9) #se guarda recorrido del nuevo camino
		j insert_6
		
	set_7:
		li $t7,8
		la $t8,268501440 
		la $t9,268501472 
		j insert_7
		
	insert_7:
		
		beqz $t7,resultado
		subi $t7,$t7,1
		addi $t8,$t8,4
		addi $s7,$s7,4
		addi $t9,$t9,4
		lw $t0,0($t8)
		bnez $t0, evaluar_7 #si el nodo siguiente es distinto de 0 debe evaluarse 
		lw $t0,0($s7)
		beqz $t0,insert_7 #si no hay valor, no tiene conexión con el nodo
		lw $t0,-8($t8) #valor acumulado de la rama 
		lw $t1,0($s7) #de otra forma se llena con el primer camino tentativo
		add $t1,$t1,$t0 #sumado al acumulado del camino
		sw $t1,0($t8)
		li $t1,7 #se marca camino con un 7
		sw $t1,0($t9)
		j insert_7
		
	evaluar_7:
		lw $t2,0($s7)
		beqz $t2,insert_7
		lw $t1,0($t8)
		add $t1,$t1,$t2 #total camino tentativo
		bge $t1,$t0, insert_7 #si el camino tentativo es mayor al guardado, no se considera
		sw $t1,0($t8) #de otra forma, se guarda el valor del nuevo camino
		li $t1,7
		sw $t1,0($t9) #se guarda recorrido del nuevo camino
		j insert_7
		
	resultado:
		la $t8,268501440 
		la $t9,268501472 
		li $v0,4
		la $a0,mensaje
		syscall
		li $v0,1
		lw $t0,28($t8)
		move $a0,$t0
		syscall
		
